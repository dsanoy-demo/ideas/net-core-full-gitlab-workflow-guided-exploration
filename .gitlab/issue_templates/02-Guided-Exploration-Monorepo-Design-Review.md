
# 1. Guided Exploration Monorepo Design Review

> **Important:** These instructions depend on the initial setup being completed first: [.gitlab/issue_templates/01-Setup-Repository.md](.gitlab/issue_templates/01-Setup-Repository.md)

## 1.1. Create an Exploration Checklist for Personal Progress Tracking
Since this document is implemented as a GitLab issue template, it can be used to create a new issue for your personal progress tracking.
The checklist steps below will become clickable (without changing to markdown edit mode) and you can have your own copy for progress.

1. * [ ] On the left navigation of this repository click **Issues**
2. * [ ] Click **New issue**
3. * [ ] On the _New Issue_ page, next to _Title_, click **Choose a template** and select **02-Guided-Exploration-Monorepo-Design-Review**
4. * [ ] The new issue created allows you to check items off as you complete them.

> **Note:** Remove the above section if creating an issue from this template.

## 1.2. GitLab Flow is Used for This Design Pattern

[GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)  follows GitOps disciplines - conseqently all changes - including configuration and deployments - must be made by using a merge request as the change and review gating mechanism for environments.

In this case there is a repository branch per environment and merges between these branches are how changes are promoted to the various environments.

[Read About GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) 

1. Merge feature branches to "Master" to collect a changeset / feature set.  No builds happen.
2. Merge master to sandbox to test release changes. (Branch is protected to only allow merges) Alternatively, feature branches could go straight to sandbox (or even make "sandbox" the default repo branch)

> Possible MR Title: "Promote changeset x.y from development to sandbox environment"

3. When merge to "sandbox" builds satisfactory, merge "sandbox" to "stage" (Branch is protected to only allow merges and only upon successful build)

> Possible MR Title: "Promote changeset x.y from sandbox to stage environment"

4. When merge to "stage" builds satisfactorily, at the designated maintenance window, merge it to "production" (Branch is protected to only allow merges and only upon successful build)

> Possible MR Title: "Promote changeset x.y from stage to production environment"

## 1.3. Guided Explorations


<!-- TOC depthfrom:3 -->

- [Observe the <u>Variable Setup</u> for Per-Deployment Environment (Branch) Configuration](#observe-the-uvariable-setupu-for-per-deployment-environment-branch-configuration)
- [Observe the <u>GitLab CI Configuration</u> for Monorepo Design](#observe-the-ugitlab-ci-configurationu-for-monorepo-design)
- [Observe a Single Sub-Directory Build](#observe-a-single-sub-directory-build)
- [Observe a Multi-Sub-Directory Build](#observe-a-multi-sub-directory-build)

<!-- /TOC -->

### 1.3.1. Observe the <u>Variable Setup</u> for Per-Deployment Environment (Branch) Configuration

The CI/CD variable in this repo meet the following unique requirements:

- Can be different per any of the three deployment environments
- Branch names map to environment names exactly (in .gitlab-ci.yml)
- Can hold secrets that cannot be seen in logs (masked) for anyone who does not have a maintainer or higher role

1. * [ ] In this repository, from the left hand navigation click **Settings => CI/CD => Variables**
2. * [ ] For _GLOBAL_VISIBLE_VAR_ note that the _Scope_ is set to _All environments_ - this means it will be the same value in all environments.
3. * [  ] For _PWD1_ note that there are three entries - one for each scope ("produciton", "sandbox" and "stage").  This is how the variables are different for each environment.  Note that these entires are also:
    *  _Protected_ - which means they cannot be changed inside of CI/CD
    *  _Masked_ - which means they will never show in clear text in logs.  This is GitLabs way of providing very lightweight security for secrets. (These can be viewed by anyone who has Maintainer or above to the repository.)
3. * [  ] For _VISIBLE_ENV_DATA_ note that there are three entries.  These are emited in the build logs to clearly demonstrate that they actually vary by environment (branch) build.
4. * [ ] From the left navigation, click **Repository => Files => .build-template.yml**
5. * [ ] Search for "environment:" and observe that the line below it is _name: $CI_COMMIT_REFNAME_.  Since this variable will be the branch name (in the cases we care about) it will map the build of a specific branch to the environment scopes you observed for variables earlier.
6. * [ ] From the left navigation, click **Operations => Environments** and observe that these environments were auto-populated by the .gitlab-ci.yml mapping that you just observed.  Environments do not need to be explicitly created before use (but they can be)

### 1.3.2. Observe the <u>GitLab CI Configuration</u> for Monorepo Design
**UNDER CONSTRUCTION**

3. * [ ] How .gitlab-ci.yml includes work
4. * [ ] gitlab includes (for enhanced control over entire repo level workflow and elimination of repeating code)
3. * [ ] limiting builds to changes in a sub-directory
3. * [ ] GitLab CI code "templates"

### 1.3.3. Observe a Single Sub-Directory Build
**UNDER CONSTRUCTION**

For when changes are only made to one sub-project.

1. * [ ] Lorem ipsum dolor sit amet

### 1.3.4. Observe a Multi-Sub-Directory Build
**UNDER CONSTRUCTION**
For when changes in one sub-project require simultaneous, dependent changes in another sub-project.

1. * [ ] Lorem ipsum dolor sit amet

